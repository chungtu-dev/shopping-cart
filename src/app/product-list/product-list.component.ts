import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
// export class ProductListComponent implements OnInit { có thể dùng implements interface hook tương ứng hoặc ko cũng được, tuy nhiên thì gọi implements interface hook đẻ cho code chặt chẽ hơn
  export class ProductListComponent {
    
  // @Input: parent -> data flow -> child
  @Input() products: any

  // @Output: child -> data flow -> parent
  @Output() onRemoveProduct = new EventEmitter();

  ngOnInit(){
    console.log('this is log from product-list-component');
  }

  removeProduct(productId: string): void { //void: ko trả về gì hết
    // const index = this.products.findIndex((p: any) => p.id === productId);
    // if (index !== -1) {
    //   this.products.splice(index, 1)
    // }

    this.onRemoveProduct.emit(productId)
  };

  updateQty(element: any): void {
    console.log(element.value); // log ra element gọi đến value
  }
}
