import { Component, OnInit } from '@angular/core';
import { Products } from './product.model';
import { PromoCode } from './promo-code.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  products: Products[] = [
    {
      id: '1',
      name: 'Iphone',
      desc: 'Iphone from apple',
      thum: 'https://cdn.minhtuanmobile.com/uploads/products/14-pro-tim-221119023329-221119143329_thumb.png',
      price: 28490000,
      qty: 1,
    },
    {
      id: '2',
      name: 'Samsung',
      desc: 'Sam the sung',
      thum: 'https://cdn.minhtuanmobile.com/uploads/products/z4191146301193-e74bc4e071d6f5a11b9ee62c74c4aa80-230318085142-230318085143_thumb.jpg',
      price: 25490000,
      qty: 1,
    }
  ];

  ngOnInit(){
    console.log('this log from app-component');
    this.updateCartSummary()
  }

  promoCodes: PromoCode[] = [
    {
      code: 'AUTUMN',
      discountPercent: 10
    },
    {
      code: 'WINTER',
      discountPercent: 20
    }
  ];

  numberItems: number = 0
  subTotal: number = 0
  discountPercent: number = 0
  discount: number = 0
  taxPercent: number = 0
  tax: number = 0

  updateCartSummary() {
    let numberItems = 0
    let subTotal = 0

    for (const product of this.products) {
      numberItems += product.qty
      subTotal += product.price * product.qty
    }

    this.numberItems = numberItems
    this.subTotal = subTotal
  }

  removeProduct(productId: string) {
    // xóa sản phẩm
    // alert('remove Product: ' + productId)
    // console.log(productId);    
    const index = this.products.findIndex((p: any) => p.id === productId);
    if (index !== -1) {
      this.products.splice(index, 1)
    }

    //tính lại tổng số lượng và tổng tiền
    this.updateCartSummary()
  }

  updateProductQty(data: { id: string, qty: number }) {
    const product = this.products.find((p) => p.id === data.id)
    if (product) {
      product.qty = data.qty
    }

    this.updateCartSummary()
  }

  handleAppyPromoCode(code: string) {
    const promoCode = this.promoCodes.find((promo)=>promo.code === code)
    this.discountPercent = promoCode ? promoCode.discountPercent : 0;
    this.discount = (this.subTotal * this.discountPercent) / 100

    if(this.discount > 0){
      alert("mã giảm giá đã được áp dụng")
    } else {
      alert("mã giảm giá không hợp lệ")
    }
  }
}


// lesson: https://www.youtube.com/playlist?list=PLlahAO-uyDzJ2tRFJ8hFkiPf6xuSg9IQa