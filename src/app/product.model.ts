export interface Products {
    id:string
    name: string;
    desc: string;
    thum: string;
    price: number;
    qty: number;
}